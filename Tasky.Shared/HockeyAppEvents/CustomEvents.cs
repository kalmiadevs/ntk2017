﻿using System;

using HockeyApp;
using System.Collections.Generic;

/*
 * DEMO #2
 * 
 * Define custom events
 * 
 */
namespace Tasky.Shared.HockeyAppEvents
{
    public static class CustomEvents
    {

        // DEMO #3
        private static String lastVisitedScreen = "";

        /*
         * send page view to hockey app
         */
		public static void SendPageView(String screenName) 
        {

			String customEventName = "Page View";
            lastVisitedScreen = screenName;

			Console.WriteLine(customEventName + ": " + screenName);

			Dictionary<string, string> properties = new Dictionary<string, string> { 
                { "screen_name", screenName } 
            };

            HockeyApp.MetricsManager.TrackEvent(customEventName, properties, null);

        }

        /*
         * send task events to hockey app
         */

		public static void SendTaskEvent(String eventName, int taskId)
		{

			String customEventName = "Task Action";

            Console.WriteLine(customEventName + ": " + eventName + " - " + taskId + " - " + lastVisitedScreen);

			Dictionary<string, string> properties = new Dictionary<string, string> { 
                { "action", eventName }, 
                { "screen", lastVisitedScreen} 
            };

            Dictionary<string, double> measurements = new Dictionary<string, double> { 
                { "task_id", taskId } 
            };

			HockeyApp.MetricsManager.TrackEvent(customEventName, properties, measurements);

		}


		/*
         * send hidden event
         */

		public static void SendHiddenEvent()
		{

			String customEventName = "Hidden Feature";

            HockeyApp.MetricsManager.TrackEvent(customEventName, null, null);

		}
    }
}
