using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using MonoTouch.Dialog;
using Foundation;
using Tasky.AL;
using Tasky.BL;

using Tasky.Shared.HockeyAppEvents;

namespace Tasky.Screens.iPhone {
	public class HomeScreen : DialogViewController {
		List<Task> tasks;

		public HomeScreen () : base (UITableViewStyle.Plain, null)
		{
			Initialize ();
		}
		
		protected void Initialize()
		{
			NavigationItem.SetRightBarButtonItem (new UIBarButtonItem (UIBarButtonSystemItem.Add), false);
			NavigationItem.RightBarButtonItem.Clicked += (sender, e) => { ShowTaskDetails(new Task()); };
		}

        public override void ViewDidAppear(Boolean animated)
		{
            base.ViewDidAppear(animated);

            // DEMO #2
            CustomEvents.SendPageView("Home");

		}

		// MonoTouch.Dialog individual TaskDetails view (uses /AL/TaskDialog.cs wrapper class)
		LocalizableBindingContext context;
		TaskDialog taskDialog;
		Task currentTask;
		DialogViewController detailsScreen;

		protected void ShowTaskDetails (Task task)
		{
			// DEMO #2
			CustomEvents.SendPageView("Task Details");

			currentTask = task;
			taskDialog = new TaskDialog (task);
			
			var title = NSBundle.MainBundle.LocalizedString ("Task Details", "Task Details");
			context = new LocalizableBindingContext (this, taskDialog, title);
			detailsScreen = new DialogViewController (context.Root, true);
			ActivateController(detailsScreen);
		}

        public void SaveTask()
        {
            context.Fetch(); // re-populates with updated values

			/*
             * DEMO #2
             * 
             * check if task was edited, checked as done or created
             */

			if (currentTask.Done != taskDialog.Done)
                CustomEvents.SendTaskEvent( (taskDialog.Done ? "Done" : "Undone") , currentTask.ID);

            if (currentTask.ID != 0 && (currentTask.Name != taskDialog.Name || currentTask.Notes != taskDialog.Notes))
                CustomEvents.SendTaskEvent("Edit", currentTask.ID);

            bool newTask = (currentTask.ID == 0);

            currentTask.Name = taskDialog.Name;
            currentTask.Notes = taskDialog.Notes;
            currentTask.Done = taskDialog.Done;

            BL.Managers.TaskManager.SaveTask(currentTask);

            if (newTask)
				CustomEvents.SendTaskEvent("Create", currentTask.ID);

			NavigationController.PopViewController (true);

		}

        public void DeleteTask()
        {

			// DEMO #2
			// delete task from home details screen

			if (currentTask.ID >= 0)
            {
                CustomEvents.SendTaskEvent("Delete", currentTask.ID);
				BL.Managers.TaskManager.DeleteTask(currentTask.ID);
            }

			NavigationController.PopViewController (true);
		}


		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			// reload/refresh
			PopulateTable();			
		}
		
		protected void PopulateTable ()
		{
			tasks = BL.Managers.TaskManager.GetTasks ().ToList ();
			var newTaskDefaultName = NSBundle.MainBundle.LocalizedString ("<new task>", "<new task>");
			// make into a list of MT.D elements to display
			List<Element> le = new List<Element>();
			foreach (var t in tasks) {
				le.Add (new CheckboxElement((t.Name == "" ? newTaskDefaultName : t.Name), t.Done));
			}

            /*
             * DEMO #4
             * 
             * add hidden feature 
             * click on first empty row will open text editor for adding new task
             * 
             */
			le.Add(new CheckboxElement("", false));

			// add to section
			var s = new Section ();
			s.AddAll (le);
			// add as root
			Root = new RootElement ("Tasky Pro") { s }; 
		}

		public override void Selected (Foundation.NSIndexPath indexPath)
		{
            if (indexPath.Row == tasks.Count)
            {
                // hidden feature
                CustomEvents.SendHiddenEvent();
            }
            else
            {
                var task = tasks[indexPath.Row];
                ShowTaskDetails(task);
            }
		}


		public override Source CreateSizingSource (bool unevenRows)
		{
			return new EditingSource (this);
		}

		public void DeleteTaskRow(int rowId)
		{
			// DEMO 2
            // delete task from home screen
			CustomEvents.SendTaskEvent("Delete", tasks[rowId].ID);
			BL.Managers.TaskManager.DeleteTask(tasks[rowId].ID);
		}
	}
}